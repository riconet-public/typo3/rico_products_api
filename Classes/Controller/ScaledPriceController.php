<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *      Created on: 17.07.2017 09:41
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoProducts\Domain\Model\FrontendUserGroup;
use Riconet\RicoProducts\Domain\Model\Price;
use Riconet\RicoProducts\Domain\Model\Product;
use Riconet\RicoProducts\Domain\Model\ScaledPrice;
use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Riconet\RicoProducts\Domain\Repository\FrontendUserGroupRepository;

/**
 * Class ScaledPriceController.
 */
class ScaledPriceController extends ApiController
{
    /**
     * ProductRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository = null;

    /**
     * FrontendUserGroupRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\FrontendUserGroupRepository
     * @inject
     */
    protected $frontendUserGroupRepository = null;

    /**
     * @var array
     */
    protected $allowedFields = [
        'articleNumber',
        'userGroupTitle',
        'quantity',
        'fixedPrice',
        'percentageDiscount',
    ];

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getRespectedStorageQuerySettings($this->storagePids);
        $this->productRepository->setDefaultQuerySettings($settings);
        $this->frontendUserGroupRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action create.
     */
    public function createAction()
    {
        // Check for arguments.
        if (!$this->request->hasArgument('scaledPrice')) {
            $this->errorResponse('missing data');

            return;
        }
        /** @var array $arguments */
        $arguments = $this->request->getArgument('scaledPrice');
        // Check for required arguments.
        if (!isset($arguments['articleNumber'], $arguments['quantity'], $arguments['userGroupTitle'])) {
            $this->errorResponse('Missing articleNumber, userGroupTitle and/or quantity');

            return;
        }
        /** @var Product $product */
        $product = $this->productRepository->findByArticleNumber($arguments['articleNumber'])->getFirst();
        // Check if product found
        if (!($product instanceof Product)) {
            $this->errorResponse('Could not find product with article number '.$arguments['articleNumber']);

            return;
        }
        /** @var FrontendUserGroup $frontendUserGroup */
        $frontendUserGroup = $this->frontendUserGroupRepository->findByTitle($arguments['userGroupTitle'])->getFirst();
        // Check if frontend user group found.
        if (!($frontendUserGroup instanceof FrontendUserGroup)) {
            $this->errorResponse('Could not find frontend user group with title '.$arguments['userGroupTitle']);

            return;
        }
        // Find or create a new pricing
        $scaledPriceContainer = null;
        /** @var Price $price */
        foreach ($product->getPrices() as $price) {
            if ($price->getFrontendUserGroup()->getUid() === $frontendUserGroup->getUid()) {
                $scaledPriceContainer = &$price;
            }
        }
        if (is_null($scaledPriceContainer)) {
            $scaledPriceContainer = GeneralUtility::makeInstance(Price::class);
            $scaledPriceContainer->setFrontendUserGroup($frontendUserGroup);
        }
        /** @var ScaledPrice $scaledPrice */
        $scaledPrice = GeneralUtility::makeInstance(ScaledPrice::class);
        // Add flat properties.
        foreach ($arguments as $key => $value) {
            if ($scaledPrice->_hasProperty($key) && in_array($key, $this->allowedFields)) {
                $scaledPrice->_setProperty($key, $value);
            }
        }
        $scaledPriceContainer->getScaledPrices()->attach($scaledPrice);
        if (!(is_numeric($scaledPriceContainer->getUid()) && $scaledPriceContainer->getUid() > 0)) {
            $product->getPrices()->attach($scaledPriceContainer);
        }
        $this->productRepository->update($product);
        $this->successResponse([], 'created scaled price');
    }

    /**
     * Action get.
     */
    public function getAction()
    {
        if (!$this->request->hasArgument('articleNumber')) {
            $this->errorResponse('missing articleNumber');

            return;
        }
        $articleNumber = $this->request->getArgument('articleNumber');
        /* @var $product Product */
        $product = $this->productRepository->findByArticleNumber($articleNumber)->getFirst();
        if (!($product instanceof Product)) {
            $this->errorResponse('Could not find product with article number '.$articleNumber);

            return;
        }

        $prices = [];
        /** @var Price $price */
        foreach ($product->getPrices() as $price) {
            $scaledPrices = [];
            /** @var ScaledPrice $scaledPrice */
            foreach ($price->getScaledPrices() as $scaledPrice) {
                $scaledPrices[] = $scaledPrice;
            }
            $prices[$price->getFrontendUserGroup()->getTitle()] = $scaledPrices;
        }
        $this->successResponse($prices);
    }

    /**
     * Action create.
     */
    public function deleteAction()
    {
        // Check for arguments.
        if (!$this->request->hasArgument('scaledPrice')) {
            $this->errorResponse('missing data');

            return;
        }
        /** @var array $arguments */
        $arguments = $this->request->getArgument('scaledPrice');
        // Check for required arguments.
        if (!isset($arguments['articleNumber'], $arguments['quantity'], $arguments['userGroupTitle'])) {
            $this->errorResponse('Missing articleNumber, userGroupTitle and/or quantity');

            return;
        }
        /** @var Product $product */
        $product = $this->productRepository->findByArticleNumber($arguments['articleNumber'])->getFirst();
        // Check if product found
        if (!($product instanceof Product)) {
            $this->errorResponse('Could not find product with article number '.$arguments['articleNumber']);

            return;
        }
        /** @var FrontendUserGroup $frontendUserGroup */
        $frontendUserGroup = $this->frontendUserGroupRepository->findByTitle($arguments['userGroupTitle'])->getFirst();
        // Check if frontend user group found.
        if (!($frontendUserGroup instanceof FrontendUserGroup)) {
            $this->errorResponse('Could not find frontend user group with title '.$arguments['userGroupTitle']);

            return;
        }
        /** @var Price $price */
        $scaledPriceToRemove = null;
        foreach ($product->getPrices() as $price) {
            if ($price->getFrontendUserGroup()->getUid() == $frontendUserGroup->getUid()) {
                /** @var ScaledPrice $scaledPrice */
                foreach ($price->getScaledPrices() as $scaledPrice) {
                    if ($scaledPrice->getQuantity() == $arguments['quantity']) {
                        $scaledPriceToRemove = $scaledPrice;
                        break;
                    }
                }
                $price->getScaledPrices()->detach($scaledPriceToRemove);
            }
        }
        if (is_null($scaledPriceToRemove)) {
            $this->errorResponse('Could not find scaled price');

            return;
        }
        $this->productRepository->update($product);
        $this->successResponse([], 'removed scaled price');
    }
}
