<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoProducts\Domain\Model\AttributeData;
use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class AttributeDataController.
 */
class AttributeDataController extends ApiController
{
    /**
     * AttributeDataRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\AttributeDataRepository
     * @inject
     */
    protected $attributeDataRepository = null;

    /**
     * AttributeRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\AttributeRepository
     * @inject
     */
    protected $attributeRepository = null;

    /**
     * ProductRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository = null;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getRespectedStorageQuerySettings($this->storagePids);
        $this->attributeRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action create.
     */
    public function createAction()
    {
        if (!$this->request->hasArgument('attributeData')) {
            $this->errorResponse('No data');

            return;
        }
        $attributeData = $this->request->getArgument('attributeData');
        if (!isset($attributeData['value'])) {
            $this->errorResponse('value');

            return;
        }
        if (isset($attributeData['attributeId'])) {
            /* @var $attribute \Riconet\RicoProducts\Domain\Model\Attribute */
            $attribute = $this->attributeRepository->findByUid($attributeData['attributeId']);
        } elseif (isset($attributeData['attributeName'])) {
            /* @var $attribute Repository|PersistenceManagerInterface|Attribute */
            $attribute = $this->attributeRepository->findByTitle($attributeData['attributeName'])->getFirst();
        } else {
            $this->errorResponse('missing attributeId or attributeName');

            return;
        }
        if ($attribute === null) {
            $this->errorResponse('attribute with this attributeId or attributeName not found');

            return;
        }
        /* @var $product \Riconet\RicoProducts\Domain\Model\Product */
        $product = $this->productRepository->findByArticleNumber($attributeData['articleNumber'])->getFirst();
        if ($product === false) {
            $this->errorResponse('product with this articleNumber doesn\'t exist');

            return;
        }
        $attributeDataModel = new AttributeData();
        $attributeDataModel->setAttribute($attribute);
        $attributeDataModel->setData($attributeData['value']);
        $this->attributeDataRepository->add($attributeDataModel);
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $persistenceManager->persistAll();
        $product->addAttribute($attributeDataModel);
        $this->productRepository->update($product);
        $this->successResponse([], 'Created attributeData');
    }

    /**
     * Action get.
     */
    public function getAction()
    {
        if (!$this->request->hasArgument('articleNumber')) {
            $this->errorResponse('missing articleNumber');

            return;
        }
        $articleNumber = $this->request->getArgument('articleNumber');
        /* @var $product \Riconet\RicoProducts\Domain\Model\Product */
        $product = $this->productRepository->findByArticleNumber($articleNumber)->getFirst();
        if ($product === null) {
            $this->errorResponse('no product with this articleNumber');

            return;
        }
        $returnArray = [];
        /* @var $attributeData \Riconet\RicoProducts\Domain\Model\AttributeData */
        foreach ($product->getAttributes() as $attributeData) {
            $returnArray[] = [
                'uid' => $attributeData->getUid(),
                'data' => $attributeData->getData(),
                'attribute' => [
                    $attributeData->getAttribute()->toArray(),
                ],
            ];
        }
        $this->successResponse($returnArray);
    }

    /**
     * Action delete.
     */
    public function deleteAction()
    {
        if (!$this->request->hasArgument('uid')) {
            $this->errorResponse('missing uid');

            return;
        }
        $attributeDataUid = $this->request->getArgument('uid');
        $attributeData = $this->attributeDataRepository->findByUid($attributeDataUid);
        if ($attributeData === null) {
            $this->errorResponse('no AttributeData with this uid (maybe deleted)');

            return;
        }
        $this->attributeDataRepository->remove($attributeData);
        $this->successResponse([], 'AttributeData Deleted');
    }
}
