<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoOrder\Domain\Model\Order;
use Riconet\RicoOrder\Domain\Repository\OrderRepository;
use Riconet\RicoProducts\Domain\Model\Product;
use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Class OrderController.
 */
class OrderController extends ApiController
{
    /**
     * orderRepository.
     *
     * @var \Riconet\RicoOrder\Domain\Repository\OrderRepository
     * @inject
     */
    protected $orderRepository = null;

    /**
     * ProductRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getUnrespectedStorageQuerySettings();
        $this->orderRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action getAll.
     */
    public function getAllAction()
    {
        $orders = $this->orderRepository->findAll();
        $data = [];
        /** @var $order Order */
        foreach ($orders as $order) {
            $entry = $order->getTransferableOrderData();
            foreach($entry['sections'] as &$section) {
                foreach($section['positions'] as &$position) {
                    $product = $this->productRepository->findByUid((int) $position['articlenumber']);
                    if ($product instanceof Product) {
                        $position['articlenumber'] = $product->getArticleNumber();
                    }
                }
            }
            $data[] = $entry;
        }
        $this->successResponse($data);
    }

    /**
     * Action getNew.
     */
    public function getNewAction()
    {
        $orders = $this->orderRepository->findByStatus($this->persistence['newStatus']);
        $data = [];
        /** @var $order Order */
        foreach ($orders as $order) {
            $entry = $order->getTransferableOrderData();
            foreach($entry['sections'] as &$section) {
                foreach($section['positions'] as &$position) {
                    $product = $this->productRepository->findByUid((int) $position['articlenumber']);
                    if ($product instanceof Product) {
                        $position['articlenumber'] = $product->getArticleNumber();
                    }
                }
            }
            $data[] = $entry;
        }
        $this->successResponse($data);
    }

    /**
     * Action update.
     */
    public function updateAction()
    {
        if (!$this->request->hasArgument('order')) {
            $this->errorResponse('missing data');

            return;
        }
        $orderArgument = $this->request->getArgument('order');
        if (!isset($orderArgument['uid'])) {
            $this->errorResponse('Missing uid');

            return;
        }
        /** @var $order Order */
        if (!($order = $this->orderRepository->findByUid((int) $orderArgument['uid']))) {
            $this->errorResponse('Can not find order with uid '.$orderArgument['uid'].'!');

            return;
        }
        // Set hidden state if set.
        if (isset($orderArgument['hidden']) && is_bool($orderArgument['hidden'])) {
            $order->setHidden(boolval($orderArgument['hidden']));
        }
        // Set status if set.
        if (isset($orderArgument['status']) && !empty(trim($orderArgument['status']))) {
            $order->setStatus(trim($orderArgument['status']));
        }
        $this->orderRepository->update($order);
        $this->successResponse([], 'updated order');
    }

    /**
     * Action delete.
     */
    public function deleteAction()
    {
        if (!($this->request->hasArgument('uid'))) {
            $this->errorResponse('missing uid');

            return;
        }
        $uid = (int) $this->request->getArgument('uid');
        if (!($order = $this->orderRepository->findByUid($uid))) {
            $this->errorResponse("Can not find order with uid $uid!");

            return;
        }
        $this->orderRepository->remove($order);
        $this->successResponse([], 'deleted order');
    }
}
