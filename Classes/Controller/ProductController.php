<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoProducts\Domain\Model\FileReference;
use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use Riconet\RicoProducts\Domain\Model\Product;

/**
 * Class ProductController.
 */
class ProductController extends ApiController
{
    /**
     * ProductRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\ProductRepository
     * @inject
     */
    protected $productRepository;

    /**
     * DiscountRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\DiscountRepository
     * @inject
     */
    protected $discountRepository;

    /**
     * FileAbstractionLayerHelper.
     *
     * @var \Riconet\RicoProductsApi\Helper\FileAbstractionLayerHelper
     * @inject
     */
    protected $falHelper = null;

    /**
     * The category repository.
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository = null;

    /**
     * @var array
     */
    protected $allowedFields = [
        'articleNumber',
        'title',
        'subtitle',
        'teaser',
        'description',
        'price',
    ];

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getRespectedStorageQuerySettings($this->storagePids);
        $this->productRepository->setDefaultQuerySettings($settings);
        $this->categoryRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action create.
     */
    public function createAction()
    {
        if (!$this->request->hasArgument('product')) {
            $this->errorResponse('missing data');

            return;
        }
        /** @var array $product */
        $product = $this->request->getArgument('product');
        if (!isset($product['title']) || !isset($product['articleNumber']) || !isset($product['price'])) {
            $this->errorResponse('Missing title, price or articleNumber');

            return;
        }
        $productModel = new Product();
        $productModel->setPid($this->defaultStoragePid);
        /* @var $queryResult \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult */
        $queryResult = $this->productRepository->findByArticleNumber($product['articleNumber']);
        if ($queryResult->count() > 0) {
            $this->errorResponse('articleNumber already exists');

            return;
        }
        // Add flat properties.
        foreach ($product as $property => $value) {
            if ($productModel->_hasProperty($property) && in_array($property, $this->allowedFields)) {
                $productModel->_setProperty($property, $value);
            }
        }
        // Add FAL images.
        if (isset($product['images']) && count($product['images']) > 0) {
            // Clear all images before add new ones.
            $productModel->getImages()->removeAll(clone $productModel->getImages());
            foreach ($product['images'] as $entry) {
                try {
                    $fileIdentifier = $entry['filename'];
                    $fal = $this->falHelper->createNewFAL($fileIdentifier);
                    if ($fal instanceof FileReference) {
                        $productModel->getImages()->attach($fal);
                    }
                } catch (\Exception $e) {
                    // TODO: handle error!
                }
            }
        }
        // Add FAL data sheets
        if (isset($product['datasheets']) && count($product['datasheets']) > 0) {
            // Clear all dataSheets before add new ones
            $productModel->getDataSheets()->removeAll(clone $productModel->getDataSheets());
            foreach ($product['datasheets'] as $entry) {
                try {
                    $fileIdentifier = $entry['filename'];
                    $fal = $this->falHelper->createNewFAL($fileIdentifier);
                    if ($fal instanceof FileReference) {
                        $productModel->getDataSheets()->attach($fal);
                    }
                } catch (\Exception $e) {
                    // TODO: handle error!
                }
            }
        }
        // Add FAL manuals
        if (isset($product['manuals']) && count($product['manuals']) > 0) {
            // Clear all manuals before add new ones.
            $productModel->getManuals()->removeAll(clone $productModel->getManuals());
            foreach ($product['manuals'] as $entry) {
                try {
                    $fileIdentifier = $entry['filename'];
                    $fal = $this->falHelper->createNewFAL($fileIdentifier);
                    if ($fal instanceof FileReference) {
                        $productModel->getManuals()->attach($fal);
                    }
                } catch (\Exception $e) {
                    // TODO: handle error!
                }
            }
        }
        // Add categories.
        if (isset($product['categories']) && count($product['categories']) > 0) {
            // Clear all categories before add new ones.
            $productModel->getCategories()->removeAll(clone $productModel->getCategories());
            foreach ($product['categories'] as $categoryTitle) {
                $category = $this->categoryRepository->findByTitle($categoryTitle)->getFirst();
                if ($category instanceof Category) {
                    $productModel->getCategories()->attach($category);
                }
            }
        }
        $this->productRepository->add($productModel);
        /* @var $persistenceManager \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager */
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $persistenceManager->persistAll();
        $this->successResponse(['productId' => $productModel->getArticleNumber()], 'created product');
    }

    /**
     * Action createDiscount.
     */
    public function createDiscountAction()
    {
        if (!$this->request->hasArgument('data')) {
            $this->errorResponse('missing data');

            return;
        }
        /** @var array $data */
        $data = $this->request->getArgument('data');
        if (!isset($data['articleNumber']) || !isset($data['discountId'])) {
            $this->errorResponse('missing article_number or discountId');

            return;
        }
        /* @var $product Product */
        $product = $this->productRepository->findByArticleNumber($data['articleNumber'])->getFirst();
        /* @var $discount \Riconet\RicoProducts\Domain\Model\Discount */
        $discount = $this->discountRepository->findByUid($data['discountId']);
        if ($product === null || $discount === null) {
            $this->errorResponse('discount or product doesn\'t exist');

            return;
        }
        $product->addDiscount($discount);
        $this->productRepository->update($product);
        $this->successResponse('Created discount for product');
    }

    /**
     * Action deleteDiscount.
     */
    public function deleteDiscountAction()
    {
        if (!$this->request->hasArgument('data')) {
            $this->errorResponse('missing data');

            return;
        }
        /** @var array $data */
        $data = $this->request->getArgument('data');
        if (!isset($data['articleNumber']) || !isset($data['discountId'])) {
            $this->errorResponse('missing article_number or discountId');

            return;
        }
        /* @var $product Product */
        $product = $this->productRepository->findByArticleNumber($data['articleNumber'])->getFirst();
        /* @var $discount \Riconet\RicoProducts\Domain\Model\Discount */
        $discount = $this->discountRepository->findByUid($data['discountId']);
        if ($product === null || $discount === null) {
            $this->errorResponse('discount or product doesn\'t exist');

            return;
        }
        $product->removeDiscount($discount);
        $this->productRepository->update($product);
        $this->successResponse([], 'Removed discount for product');
    }

    /**
     * Action update.
     */
    public function updateAction()
    {
        if (!$this->request->hasArgument('product')) {
            $this->errorResponse('missing data');

            return;
        }
        /** @var array $product */
        $product = $this->request->getArgument('product');
        if (!isset($product['articleNumber'])) {
            $this->errorResponse('Missing articleNumber');

            return;
        }
        /* @var $dbProduct \Riconet\RicoProducts\Domain\Model\Product */
        $dbProduct = $this->productRepository->findByArticleNumber($product['articleNumber'])->getFirst();
        foreach ($product as $property => $value) {
            if ($property == 'articleNumber') {
                continue;
            }
            if ($dbProduct->_hasProperty($property) && in_array($property, $this->allowedFields)) {
                $dbProduct->_setProperty($property, $value);
            }
        }
        // Add FAL images.
        if (isset($product['images']) && count($product['images']) > 0) {
            // Clear all images before add new ones.
            $dbProduct->getImages()->removeAll(clone $dbProduct->getImages());
            foreach ($product['images'] as $entry) {
                try {
                    $fileIdentifier = $entry['filename'];
                    $fal = $this->falHelper->createNewFAL($fileIdentifier);
                    if ($fal instanceof FileReference) {
                        $dbProduct->getImages()->attach($fal);
                    }
                } catch (\Exception $e) {
                    // TODO: handle error!
                }
            }
        }
        // Add FAL data sheets
        if (isset($product['datasheets']) && count($product['datasheets']) > 0) {
            // Clear all images before add new ones.
            $dbProduct->getDataSheets()->removeAll(clone $dbProduct->getDataSheets());
            foreach ($product['datasheets'] as $entry) {
                try {
                    $fileIdentifier = $entry['filename'];
                    $fal = $this->falHelper->createNewFAL($fileIdentifier);
                    if ($fal instanceof FileReference) {
                        $dbProduct->getDataSheets()->attach($fal);
                    }
                } catch (\Exception $e) {
                    // TODO: handle error!
                }
            }
        }
        // Add FAL manuals
        if (isset($product['manuals']) && count($product['manuals']) > 0) {
            // Clear all images before add new ones.
            $dbProduct->getManuals()->removeAll(clone $dbProduct->getManuals());
            foreach ($product['manuals'] as $entry) {
                try {
                    $fileIdentifier = $entry['filename'];
                    $fal = $this->falHelper->createNewFAL($fileIdentifier);
                    if ($fal instanceof FileReference) {
                        $dbProduct->getManuals()->attach($fal);
                    }
                } catch (\Exception $e) {
                    // TODO: handle error!
                }
            }
        }
        // Add categories.
        if (isset($product['categories'])) {
            // Clear all categories before add new ones.
            $dbProduct->getCategories()->removeAll(clone $dbProduct->getCategories());
            foreach ($product['categories'] as $categoryTitle) {
                $category = $this->categoryRepository->findByTitle($categoryTitle)->getFirst();
                if ($category instanceof Category) {
                    $dbProduct->getCategories()->attach($category);
                }
            }
        }
        $this->productRepository->update($dbProduct);
        $this->successResponse([], 'updated product');
    }

    /**
     * Action get.
     */
    public function getAction()
    {
        if (!$this->request->hasArgument('articleNumber')) {
            $this->errorResponse('missing articleNumber');

            return;
        }
        $articleNumber = $this->request->getArgument('articleNumber');
        /* @var $product Product */
        $product = $this->productRepository->findByArticleNumber($articleNumber)->getFirst();
        if ($product === null) {
            $this->errorResponse('article doesn\'t exist (maybe deleted)');

            return;
        }
        $this->successResponse($product);
    }

    /**
     * Action delete.
     */
    public function deleteAction()
    {
        if (!$this->request->hasArgument('articleNumber')) {
            $this->errorResponse('missing articleNumber');

            return;
        }
        $articleNumber = $this->request->getArgument('articleNumber');
        /* @var $product \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult */
        $product = $this->productRepository->findByArticleNumber($articleNumber)->getFirst();
        if ($product === null) {
            $this->errorResponse('article doesn\'t exist (maybe deleted)');

            return;
        }
        $this->productRepository->remove($product);
        $this->successResponse([], 'deleted product');
    }
}
