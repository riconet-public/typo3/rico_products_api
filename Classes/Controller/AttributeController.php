<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationBuilder;
use TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Mvc\Controller\MvcPropertyMappingConfiguration;
use Riconet\RicoProducts\Domain\Model\Attribute;

/**
 * Class AttributeController.
 */
class AttributeController extends ApiController
{
    /**
     * AttributeRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\AttributeRepository
     * @inject
     */
    protected $attributeRepository;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getRespectedStorageQuerySettings($this->storagePids);
        $this->attributeRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action create.
     *
     * @param Attribute $attribute
     */
    public function createAction(Attribute $attribute)
    {
        /* @var $queryResult \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult */
        $queryResult = $this->attributeRepository->findByTitle($attribute->getTitle());
        if ($queryResult->count() > 0) {
            $this->errorResponse('attribute already exists');

            return;
        }
        $this->attributeRepository->add($attribute);
        /* @var $persistenceManager \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager */
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $persistenceManager->persistAll();
        $this->successResponse([], 'created attribute '.$attribute->getUid());
    }

    /**
     * Action getAll.
     */
    public function getAllAction()
    {
        $attributes = $this->attributeRepository->findAll()->toArray();
        $this->successResponse($attributes, '');
    }

    /**
     * Action get.
     */
    public function getAction()
    {
        if (!$this->request->hasArgument('uid')) {
            $this->errorResponse('missing attribute uid');

            return;
        }
        $attributeUid = $this->request->getArgument('uid');
        /* @var $attribute Attribute */
        $attribute = $this->attributeRepository->findByUid($attributeUid);
        if ($attribute === null) {
            $this->errorResponse('attribute doesn\'t exist (maybe deleted)');

            return;
        }
        $this->successResponse($attribute);
    }

    /**
     * Action delete.
     */
    public function deleteAction()
    {
        if (!$this->request->hasArgument('uid')) {
            $this->errorResponse('missing attribute uid');

            return;
        }
        $attributeUid = $this->request->getArgument('uid');
        /* @var $attribute Attribute */
        $attribute = $this->attributeRepository->findByUid($attributeUid);
        if ($attribute === null) {
            $this->errorResponse('attribute doesn\'t exist (maybe deleted)');

            return;
        }
        $this->attributeRepository->remove($attribute);
        $this->successResponse([], 'deleted attribute');
    }

    /**
     * addPropertyMappingConfiguration.
     */
    protected function addPropertyMappingConfiguration()
    {
        if ($this->request->hasArgument('attribute')) {
            $propertyMappingConfiguration = (new PropertyMappingConfigurationBuilder())->build(MvcPropertyMappingConfiguration::class);
            $propertyMappingConfiguration->setTypeConverterOption(PersistentObjectConverter::class, PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED, true);
            foreach ($this->request->getArgument('attribute') as $propertyName => $value) {
                $propertyMappingConfiguration->allowProperties($propertyName);
            }
            $this->arguments->getArgument('attribute')->injectPropertyMappingConfiguration($propertyMappingConfiguration);
        }
    }
}
