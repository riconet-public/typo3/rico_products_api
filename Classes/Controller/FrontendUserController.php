<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;

/**
 * Class FrontendUserController.
 */
class FrontendUserController extends ApiController
{
    /**
     * The allowed fields.
     *
     * @var array
     */
    protected $allowedFields = [
        'address',
        'city',
        'company',
        'country',
        'email',
        'fax',
        'firstName',
        'lastName',
        'middleName',
        'name',
        'password',
        'telephone',
        'title',
        'www',
        'zip',
        'username',
    ];

    /**
     * frontendUserRepository.
     *
     * @var \TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository = null;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getRespectedStorageQuerySettings($this->storagePids);
        $this->frontendUserRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action create.
     */
    public function createAction()
    {
        if (!$this->request->hasArgument('frontendUser')) {
            $this->errorResponse('missing data');

            return;
        }
        $frontendUser = $this->request->getArgument('frontendUser');
        if (!isset($frontendUser['email']) || !isset($frontendUser['firstName']) || !isset($frontendUser['lastName'])) {
            $this->errorResponse('missing email, firstName or lastName');

            return;
        }
        $email_check = $this->frontendUserRepository->findByEmail($frontendUser['email'])->getFirst();
        if ($email_check !== null) {
            $this->errorResponse('email already exists');

            return;
        }
        $frontendUser_model = new FrontendUser();
        foreach ($frontendUser as $property => $value) {
            if ($frontendUser_model->_hasProperty($property) && in_array($property, $this->allowedFields)) {
                $frontendUser_model->_setProperty($property, $value);
            }
        }
        $this->frontendUserRepository->add($frontendUser_model);
        /* @var $persistenceManager \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager */
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $persistenceManager->persistAll();
        $this->successResponse(['uid' => $frontendUser_model->getUid()], 'created frontendUser');
    }

    /**
     * Action get.
     */
    public function getAction()
    {
        if ($this->request->hasArgument('uid')) {
            // Handle uid.
            $uid = $this->request->getArgument('uid');

            /* @var $frontendUser FrontendUser */
            $frontendUser = $this->frontendUserRepository->findByUid($uid);
            if ($frontendUser === null) {
                $this->errorResponse('user does not exist (maybe deleted)');

                return;
            }
            $this->successResponse($frontendUser);
        } else if ($this->request->hasArgument('email')) {
            // Handle email.
            $email = $this->request->getArgument('email');

            /* @var $frontendUser FrontendUser */
            $frontendUser = $this->frontendUserRepository->findByEmail($email)->getFirst();
            if ($frontendUser === null) {
                $this->errorResponse("user with email $email does not exist (maybe deleted)");

                return;
            }
            $this->successResponse($frontendUser);
        } else {
            $this->errorResponse('missing attribute uid/email');

            return;
        }
    }

    /**
     * Action getAll.
     */
    public function getAllAction()
    {
        $frontendUsers = $this->frontendUserRepository->findAll();
        if (!($frontendUsers->count() > 0)) {
            $this->errorResponse('No users found!');

            return;
        }
        $this->successResponse($frontendUsers);
    }

    /**
     * Action delete.
     */
    public function deleteAction()
    {
        if (!$this->request->hasArgument('uid')) {
            $this->errorResponse('missing attribute uid');

            return;
        }
        $uid = $this->request->getArgument('uid');
        $frontendUser = $this->frontendUserRepository->findByUid($uid);
        if ($frontendUser === null) {
            $this->errorResponse('frontendUSer doesn\'t exist (maybe deleted)');

            return;
        }
        $this->frontendUserRepository->remove($frontendUser);
        $this->successResponse([], 'deleted frontendUser');
    }

    /**
     * Action update.
     */
    public function updateAction()
    {
        if (!$this->request->hasArgument('frontendUser')) {
            $this->errorResponse('missing data');

            return;
        }
        $frontendUser = $this->request->getArgument('frontendUser');
        if (!isset($frontendUser['uid'])) {
            $this->errorResponse('missing uid');

            return;
        }
        $frontendUserDb = $this->frontendUserRepository->findByUid($frontendUser['uid']);
        if ($frontendUserDb === null) {
            $this->errorResponse('frontendUSer doesn\'t exist (maybe deleted)');

            return;
        }
        foreach ($frontendUser as $property => $value) {
            if ($frontendUserDb->_hasProperty($property) && in_array($property, $this->allowedFields)) {
                $frontendUserDb->_setProperty($property, $value);
            }
        }
        $this->frontendUserRepository->update($frontendUserDb);
        $this->successResponse([], 'updated frontendUser');
    }
}
