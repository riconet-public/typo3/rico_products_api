<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class ApiController.
 */
class ApiController extends ActionController
{
    /**
     * Status code for success.
     */
    const STATUS_SUCCESS = 0;

    /**
     * Status code for error.
     */
    const STATUS_ERROR = 1;

    /**
     * view.
     *
     * @var \TYPO3\CMS\Extbase\Mvc\View\JsonView
     */
    protected $view;

    /**
     * defaultViewObjectName.
     *
     * @var string
     */
    protected $defaultViewObjectName = JsonView::class;

    /**
     * @var array
     */
    protected $persistence = [];

    /**
     * @var array
     */
    protected $storagePids = [];

    /**
     * @var int
     */
    protected $defaultStoragePid = 0;

    /**
     * Build a success response.
     *
     * @param $data
     * @param string $message
     */
    protected function successResponse($data, $message = '')
    {
        $this->buildResponse(self::STATUS_SUCCESS, $data, $message);
    }

    /**
     * Build an error response.
     *
     * @param $message
     */
    protected function errorResponse($message)
    {
        $this->buildResponse(self::STATUS_ERROR, [], $message);
    }

    /**
     * Build a response.
     *
     * @param $status
     * @param array  $data
     * @param string $message
     */
    protected function buildResponse($status, $data = [], $message = '')
    {
        $dataObject = [
            'success' => ($status == self::STATUS_SUCCESS) ? true : false,
            'data' => $data,
            'message' => $message,
        ];
        $this->view->assign('value', $dataObject);
    }

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        // if the request does not come from a fluid form:
        // * the properties which are allowed to map must be set manually
        if (!$this->request->getInternalArgument('__trustedProperties')) {
            $this->addPropertyMappingConfiguration();
        }
        // Get the default storage pid from.
        $this->persistence = $this->configurationManager->getConfiguration(
            ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK
        )['persistence'];
        $this->storagePids = GeneralUtility::intExplode(',', $this->persistence['storagePid']);
        if (isset($this->storagePids[0])) {
            $this->defaultStoragePid = $this->storagePids[0];
        }
    }

    /**
     * Define a custom property mapping configuration.
     */
    protected function addPropertyMappingConfiguration()
    {
    }
}
