<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Controller;

use Riconet\RicoProductsApi\Utility\RepositoryUtility;
use TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationBuilder;
use TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter;
use TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager;
use TYPO3\CMS\Extbase\Mvc\Controller\MvcPropertyMappingConfiguration;
use Riconet\RicoProducts\Domain\Model\Discount;

/**
 * Class DiscountController.
 */
class DiscountController extends ApiController
{
    /**
     * discountRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\DiscountRepository
     * @inject
     */
    protected $discountRepository;

    /**
     * Action initialize.
     */
    public function initializeAction()
    {
        parent::initializeAction();
        $settings = RepositoryUtility::getRespectedStorageQuerySettings($this->storagePids);
        $this->discountRepository->setDefaultQuerySettings($settings);
    }

    /**
     * Action create.
     *
     * @param Discount $discount
     */
    public function createAction(Discount $discount)
    {
        if ($discount->getFixedValue() + $discount->getPercentValue() <= 0) {
            $this->errorResponse('fixedValue or percentValue must be given and must be greater then 0');

            return;
        }
        if ($discount->getFixedValue() > 0) {
            /* @var $queryResult \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult */
            $queryResult = $this->discountRepository->findByFixedValue($discount->getFixedValue());
            if ($queryResult->count() > 0) {
                $this->errorResponse('discount already exists');

                return;
            }
        }
        if ($discount->getPercentValue() > 0) {
            /* @var $queryResult \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult */
            $queryResult = $this->discountRepository->findByPercentValue($discount->getPercentValue());
            if ($queryResult->count() > 0) {
                $this->errorResponse('discount already exists');

                return;
            }
        }
        $discount->setPid($this->defaultStoragePid);
        $this->discountRepository->add($discount);
        /* @var $persistenceManager \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager */
        $persistenceManager = $this->objectManager->get(PersistenceManager::class);
        $persistenceManager->persistAll();
        $this->successResponse([], 'created discount '.$discount->getUid());
    }

    /**
     * Action get all.
     */
    public function getAllAction()
    {
        $discounts = $this->discountRepository->findAll()->toArray();
        $this->successResponse($discounts, '');
    }

    /**
     * Action get.
     */
    public function getAction()
    {
        if (!$this->request->hasArgument('uid')) {
            $this->errorResponse('missing discount uid');

            return;
        }
        $discountUid = $this->request->getArgument('uid');
        /* @var $discount Discount */
        $discount = $this->discountRepository->findByUid($discountUid);

        if ($discount === null) {
            $this->errorResponse('discount doesn\'t exist (maybe deleted)');

            return;
        }
        $this->successResponse($discount);
    }

    /**
     * Action delete.
     */
    public function deleteAction()
    {
        if (!$this->request->hasArgument('uid')) {
            $this->errorResponse('missing attribute uid');

            return;
        }
        $discountUid = $this->request->getArgument('uid');
        /* @var $discount Discount */
        $discount = $this->discountRepository->findByUid($discountUid);
        if ($discount === null) {
            $this->errorResponse('discount doesn\'t exist (maybe deleted)');

            return;
        }
        $this->discountRepository->remove($discount);
        $this->successResponse([], 'deleted discount');
    }

    /**
     * addPropertyMappingConfiguration.
     */
    protected function addPropertyMappingConfiguration()
    {
        if ($this->request->hasArgument('discount')) {
            $propertyMappingConfiguration = (new PropertyMappingConfigurationBuilder())->build(
                MvcPropertyMappingConfiguration::class
            );
            $propertyMappingConfiguration->setTypeConverterOption(
                PersistentObjectConverter::class,
                PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            );
            foreach ($this->request->getArgument('discount') as $propertyName => $value) {
                $propertyMappingConfiguration->allowProperties($propertyName);
            }
            $this->arguments->getArgument('discount')->injectPropertyMappingConfiguration($propertyMappingConfiguration);
        }
    }
}
