<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *      Created on: 03.07.2017 14:35
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Helper;

use Riconet\RicoProducts\Domain\Model\FileReference;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class FileAbstractionLayerHelper.
 */
class FileAbstractionLayerHelper
{
    /**
     * FileReferenceRepository.
     *
     * @var \Riconet\RicoProducts\Domain\Repository\FileReferenceRepository
     * @inject
     */
    protected $falRepository = null;

    /**
     * The identifier of the storage.
     * Default is 1 (fileadmin).
     *
     * @var string
     */
    protected $storageIdentifier = '1';

    /**
     * ResourceFactory.
     *
     * @var ResourceFactory
     */
    protected $resourceFactory = null;

    /**
     * FileAbstractionLayerHelper constructor.
     */
    public function __construct()
    {
        $this->initialize();
    }

    /**
     * initialize FileAbstractionLayerHelper.
     */
    public function initialize()
    {
        $this->resourceFactory = ResourceFactory::getInstance();
    }

    /**
     * Create a new file abstraction layer by a given file.
     *
     * @param $fileIdentifier
     *
     * @return FileReference|null
     */
    public function createNewFAL($fileIdentifier)
    {
        try {
            $file = $this->resourceFactory->getFileObjectFromCombinedIdentifier(
                $this->storageIdentifier.':/'.trim($fileIdentifier, '/')
            );
        } catch (\Exception $e) {
            // Show a better error message.
            return null;
        }
        /** @var array $metaData */
        $metaData = $file->_getMetaData(); // TODO: Internal, Search for a better solution!
        /** @var FileReference $newFAL */
        $newFAL = GeneralUtility::makeInstance(FileReference::class);
        $newFAL->setFile($file);
        $newFAL->setAlternative($metaData['alternative']); // TODO: Bad practice... Search for a better solution!
        $newFAL->setDescription($metaData['description']); // TODO: Bad practice... Search for a better solution!
//        $this->falRepository->add($newFAL); // Maybe useless or even results in problems!

        return $newFAL;
    }

    /**
     * Sets the storage.
     *
     * @param string $identifier
     *
     * @throws \Exception
     */
    public function setStorage($identifier)
    {
        $storage = $this->resourceFactory->getStorageObject((int) $identifier);
        if ($storage instanceof ResourceStorage) {
            $this->storageIdentifier = $identifier;
        } else {
            throw new \Exception("Storage with given identifier *$identifier* not found!");
        }
    }
}
