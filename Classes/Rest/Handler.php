<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Rest;

use Cundd\Rest\Handler\HandlerInterface;
use Cundd\Rest\Http\Header;
use Cundd\Rest\Http\RestRequestInterface;
use Cundd\Rest\Router\Route;
use Cundd\Rest\Router\RouterInterface;
use TYPO3\CMS\Extbase\Core\Bootstrap;

/**
 * Class Handler.
 */
class Handler implements HandlerInterface
{
    /**
     * @var \Cundd\Rest\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    /**
     * @var \Cundd\Rest\ResponseFactoryInterface
     * @inject
     */
    protected $responseFactory;

    /**
     * {@inheritdoc}
     */
    public function configureRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        $router->add(
            Route::get(
                $request->getResourceType(),
                function (RestRequestInterface $request) {
                    return array(
                        'path' => $request->getPath(),
                        'uri' => (string) $request->getUri(),
                        'resourceType' => (string) $request->getResourceType(),
                    );
                }
            )
        );
        $this->registerProductRoutes($router, $request);
        $this->registerAttributeRoutes($router, $request);
        $this->registerDiscountRoutes($router, $request);
        $this->registerAttributeDataRoutes($router, $request);
        $this->registerFrontendUserRoutes($router, $request);
        $this->registerOrderRoutes($router, $request);
        $this->registerScaledPriceRoutes($router, $request);

    }

    /**
     * Register product routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerProductRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        // Create product.
        $router->add(
            Route::post(
                $request->getResourceType().'/product/create',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'product' => $request->getSentData(),
                    ];

                    return $this->callAction('Product', 'create', $arguments);
                }
            )
        );
        // Get product.
        $router->add(
            Route::get(
                $request->getResourceType().'/product/{string}',
                function (RestRequestInterface $request, $articleNumber) {
                    $arguments = [
                        'articleNumber' => $articleNumber,
                    ];

                    return $this->callAction('Product', 'get', $arguments);
                }
            )
        );
        // Update product.
        $router->add(
            Route::post(
                $request->getResourceType().'/product/update',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'product' => $request->getSentData(),
                    ];

                    return $this->callAction('Product', 'update', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/product/delete/{string}',
                function (RestRequestInterface $request, $articleNumber) {
                    $arguments = [
                        'articleNumber' => $articleNumber,
                    ];

                    return $this->callAction('Product', 'delete', $arguments);
                }
            )
        );
        // Add create product discount route.
        $router->add(
            Route::post(
                $request->getResourceType().'/product/createDiscount',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'data' => $request->getSentData(),
                    ];

                    return $this->callAction('Product', 'createDiscount', $arguments);
                }
            )
        );
        // Add delete product discount
        $router->add(
            Route::post(
                $request->getResourceType().'/product/deleteDiscount',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'data' => $request->getSentData(),
                    ];

                    return $this->callAction('Product', 'deleteDiscount', $arguments);
                }
            )
        );
    }

    /**
     * Register attribute routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerAttributeRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        $router->add(
            Route::post(
                $request->getResourceType().'/attribute/create',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'attribute' => $request->getSentData(),
                    ];

                    return $this->callAction('Attribute', 'create', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/attribute/getAll',
                function (RestRequestInterface $request) {
                    return $this->callAction('Attribute', 'getAll', []);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/attribute/',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'attribute' => $request->getSentData(),
                    ];

                    return $this->callAction('Attribute', 'get', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/attribute/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('Attribute', 'get', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/attribute/delete/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('Attribute', 'delete', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/attribute/delete',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'attribute' => $request->getSentData(),
                    ];

                    return $this->callAction('Attribute', 'delete', $arguments);
                }
            )
        );
    }

    /**
     * Register attribute data routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerAttributeDataRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        $router->add(
            Route::post(
                $request->getResourceType().'/attributeData/create',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'attributeData' => $request->getSentData(),
                    ];

                    return $this->callAction('AttributeData', 'create', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/attributeData/{string}',
                function (RestRequestInterface $request, $articleNumber) {
                    $arguments = [
                        'articleNumber' => $articleNumber,
                    ];

                    return $this->callAction('AttributeData', 'get', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/attributeData/delete/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('AttributeData', 'delete', $arguments);
                }
            )
        );
    }

    /**
     * Register discount routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerDiscountRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        $router->add(
            Route::post(
                $request->getResourceType().'/discount/create',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'discount' => $request->getSentData(),
                    ];

                    return $this->callAction('Discount', 'create', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/discount/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('Discount', 'get', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/discount/',
                function (RestRequestInterface $request) {
                    return $this->callAction('Discount', 'getAll', []);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/discount/delete/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('Discount', 'delete', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/discount/delete',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'discount' => $request->getSentData(),
                    ];

                    return $this->callAction('Discount', 'delete', $arguments);
                }
            )
        );
    }

    /**
     * Register frontend user routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerFrontendUserRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        $router->add(
            Route::post(
                $request->getResourceType().'/user/create',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'frontendUser' => $request->getSentData(),
                    ];

                    return $this->callAction('FrontendUser', 'create', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/user/update',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'frontendUser' => $request->getSentData(),
                    ];

                    return $this->callAction('FrontendUser', 'update', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/user/delete/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('FrontendUser', 'delete', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/user/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('FrontendUser', 'get', $arguments);
                }
            )
        );
        $router->add(
            Route::post(
                $request->getResourceType().'/user',
                function (RestRequestInterface $request) {
                    $arguments = $request->getSentData();

                    return $this->callAction('FrontendUser', 'get', $arguments);
                }
            )
        );
        $router->add(
            Route::get(
                $request->getResourceType().'/user/getAll',
                function (RestRequestInterface $request) {
                    return $this->callAction('FrontendUser', 'getAll', []);
                }
            )
        );
    }

    /**
     * Register order routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerOrderRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        // Get all orders.
        $router->add(
            Route::get(
                $request->getResourceType().'/orders/getAll',
                function (RestRequestInterface $request) {
                    return $this->callAction('Order', 'getAll', []);
                }
            )
        );
        // Get all new orders.
        $router->add(
            Route::get(
                $request->getResourceType().'/orders',
                function (RestRequestInterface $request) {
                    return $this->callAction('Order', 'getNew', []);
                }
            )
        );
        // Update order status.
        $router->add(
            Route::post(
                $request->getResourceType().'/orders/update',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'order' => $request->getSentData(),
                    ];

                    return $this->callAction('Order', 'update', $arguments);
                }
            )
        );
        // Remove an order.
        $router->add(
            Route::post(
                $request->getResourceType().'/orders/delete/{int}',
                function (RestRequestInterface $request, $uid) {
                    $arguments = [
                        'uid' => $uid,
                    ];

                    return $this->callAction('Order', 'delete', $arguments);
                }
            )
        );
    }

    /**
     * Register scaled price routes.
     *
     * @param RouterInterface      $router
     * @param RestRequestInterface $request
     */
    private function registerScaledPriceRoutes(RouterInterface $router, RestRequestInterface $request)
    {
        // Create a new scaled price.
        $router->add(
            Route::post(
                $request->getResourceType().'/scaled-price/create',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'scaledPrice' => $request->getSentData(),
                    ];

                    return $this->callAction('ScaledPrice', 'create', $arguments);
                }
            )
        );
        // Gets all scaled prices of a product.
        $router->add(
            Route::get(
                $request->getResourceType().'/scaled-price/{string}',
                function (RestRequestInterface $request, $articleNumber) {
                    $arguments = [
                        'articleNumber' => $articleNumber,
                    ];

                    return $this->callAction('ScaledPrice', 'get', $arguments);
                }
            )
        );
        // Deletes a scaled price.
        $router->add(
            Route::post(
                $request->getResourceType().'/scaled-price/delete',
                function (RestRequestInterface $request) {
                    $arguments = [
                        'scaledPrice' => $request->getSentData(),
                    ];

                    return $this->callAction('ScaledPrice', 'delete', $arguments);
                }
            )
        );
    }

    /**
     * Call action.
     *
     * @param $controller
     * @param $action
     * @param $arguments
     *
     * @return string
     */
    public function callAction($controller, $action, $arguments)
    {
        return $this->callExtbasePlugin(
            'products_api',
            'Riconet',
            'RicoProductsApi',
            $controller,
            $action,
            $arguments
        );
    }

    /**
     * Calls an extbase plugin.
     *
     * @param string $pluginName     the name of the plugin like configured in ext_localconf.php
     * @param string $vendorName     the name of the vendor (if no vendor use '')
     * @param string $extensionName  the name of the extension
     * @param string $controllerName the name of the controller
     * @param string $actionName     the name of the action to call
     * @param array  $arguments      the arguments to pass to the action
     *
     * @return string
     */
    protected function callExtbasePlugin($pluginName, $vendorName, $extensionName, $controllerName, $actionName, $arguments)
    {
        $pluginNamespace = strtolower('tx_'.$extensionName.'_'.$pluginName);
        $_POST[$pluginNamespace]['controller'] = $controllerName;
        $_POST[$pluginNamespace]['action'] = $actionName;
        $keys = array_keys($arguments);
        foreach ($keys as $key) {
            $_POST[$pluginNamespace][$key] = $arguments[$key];
        }
        $configuration = [
            'extensionName' => $extensionName,
            'pluginName' => $pluginName,
        ];
        if (!empty($vendorName)) {
            $configuration['vendorName'] = $vendorName;
        }
        $bootstrap = $this->objectManager->get(Bootstrap::class);
        $response = $bootstrap->run('', $configuration);
        $response = $this->responseFactory->createResponse($response, 200)->withAddedHeader(Header::CONTENT_TYPE, 'application/json; charset=utf-8');

        return $response;
    }
}
