<?php
/***************************************************************
 *  Copyright notice
 *
 *  (c) 2017 Wolf Utz <utz@riconet.de>, riconet
 *      Created on: 27.06.2017 14:06
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace Riconet\RicoProductsApi\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Persistence\Generic\QuerySettingsInterface;

/**
 * Class RepositoryUtility.
 */
class RepositoryUtility
{
    /**
     * Gets untouched query settings.
     *
     * @return object|QuerySettingsInterface
     */
    public static function getQuerySettings()
    {
        /** @var $objectManager ObjectManager */
        $objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        /* @var $querySettings QuerySettingsInterface */
        return $objectManager->get(QuerySettingsInterface::class);
    }

    /**
     * Gets query settings which doesnt respect the storage page.
     *
     * @return object|QuerySettingsInterface
     */
    public static function getUnrespectedStorageQuerySettings()
    {
        $querySettings = self::getQuerySettings();
        $querySettings->setRespectStoragePage(false);

        return $querySettings;
    }

    /**
     * Gets query settings which does respect the given storage page ids.
     *
     * @param array $pids
     *
     * @return object|QuerySettingsInterface
     */
    public static function getRespectedStorageQuerySettings(array $pids)
    {
        $querySettings = self::getQuerySettings();
        $querySettings->setStoragePageIds($pids);

        return $querySettings;
    }
}
