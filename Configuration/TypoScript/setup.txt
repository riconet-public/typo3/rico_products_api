# Plugin setup.
plugin.tx_ricoproductsapi {
    persistence {
        storagePid = {$plugin.tx_ricoproductsapi.persistence.storagePid}
        newStatus = {$plugin.tx_ricoproductsapi.persistence.newStatus}
    }
}
# API configuration
plugin.tx_rest.settings {
    paths {
        riconet-rico_products_api {
            path = riconet-rico_products_api-*
            read = require
            write = require
        }
    }
    aliases {
        products = riconet-rico_products_api-custom_handler
    }
}